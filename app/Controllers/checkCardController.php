<?php

namespace App\Controllers;

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use PDO;

class checkCardController extends Controller
{
  public function convertWord($data)
    {
      if($data == "1" || $data == "MAN") {
        $strData = "M";
      }elseif($data == "2" || $data == "WOMAN" || $data == "W") {
        $strData = "F";
      }else {
        $strData = str_replace(' ','',$data);
      }
      return $strData;
    }

	public function checkCard(ServerRequestInterface $request, ResponseInterface $response)
    {
      error_reporting(E_ALL & ~E_NOTICE);
      $temparray = array();
      $sql_checkCard = $this->container->db->query("SELECT * FROM kmutt_member.member_reader");
      $sql_checkCard->execute();

      foreach($sql_checkCard->fetchAll(PDO::FETCH_OBJ) AS $row) {
        $address = $this->convertWord($row->reader_address);
        $sex = $this->convertWord($row->reader_sex);

        $temparray[] = array(
          'reader_id' => $row->reader_id,
          'reader_card_id' => $row->reader_card_id,
          'reader_reader_uid' => $row->reader_reader_uid,
          'reader_prefix_en' => $row->reader_prefix_en,
          'reader_patron_fname_en' => $row->reader_patron_fname_en,
          'reader_patron_lname_en' => $row->reader_patron_lname_en,
          'reader_prefix_th' => $row->reader_prefix_th,
          'reader_patron_fname_th' => $row->reader_patron_fname_th,
          'reader_patron_lname_th' => $row->reader_patron_lname_th,
          'reader_birthday' => $row->reader_birthday,
          'reader_sex' => $sex,
          'reader_issue' => $row->reader_issue,
          'reader_expire' => $row->reader_expire,
          'reader_img_card' => $row->reader_img_card,
          'reader_create_at' => $row->reader_create_at,
          'reader_address' => $address,
          'reader_status' => $row->reader_status
        );

      }

      $response = $this->response->withJson($temparray);
      return $response;
      //$result = $response->withRedirect($this->container->router->pathFor('newreleases',$temparray));
      //return response()->json(['result' => $result[0]]);
  }
}